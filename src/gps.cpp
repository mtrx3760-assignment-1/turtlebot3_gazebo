#include "turtlebot3_gazebo/gps.h"
#include "ros/ros.h"

GpsLocation::GpsLocation()
    : nh_priv("~")
{
    ROS_INFO("Location Node Activated");
    auto ret = init();
    ROS_ASSERT(ret);
}    
GpsLocation::~GpsLocation()
{
}

bool GpsLocation::init()
{
    //initialize publishers
    location_pub = nh_.advertise<turtlebot3_gazebo::tb3location>("current_location",1);

    //initialize subscribers
    tag_detection_sub = nh_.subscribe("tag_detections",1,&GpsLocation::TagDetectionCallBack, this);
    
    tag_id = 0;
    pose_x = 0;

    return true;
}

void GpsLocation::TagDetectionCallBack(const apriltag_ros::AprilTagDetectionArray::ConstPtr &msg){
        if(msg->detections.size()!=0)
        {
            tag_id = msg->detections[0].id[0];
            pose_x = msg->detections[0].pose.pose.pose.position.x;
            pose_y = msg->detections[0].pose.pose.pose.position.y;
            // header = msg->header;
            BuildingDetection();
        }
}

void GpsLocation::BuildingDetection(){
    if(tag_id>=60&&tag_id<=68){
        building_id = tag_id-60;
    }
    proximity = sqrt(pose_x*pose_x + pose_y*pose_y);
    proximity = 65-(proximity*100);
    UpdateLocation();
}
void GpsLocation::UpdateLocation(){
    turtlebot3_gazebo::tb3location current_location;
    current_location.buildingId = building_id;
    current_location.proximity = proximity;

    if(building_id==0)
        ROS_INFO("%0.2lf meters close to Restaurant",proximity);
    else       
        ROS_INFO("%0.2lf meters close to House Addr: %d",proximity,building_id);

    location_pub.publish(current_location);
}

