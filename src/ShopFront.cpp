#include "ros/ros.h"
#include "turtlebot3_gazebo/pizzaOrder.h"
#include "turtlebot3_gazebo/ShopFront.h"
#include <iostream>
#include <queue>
using namespace std;

ShopFront::ShopFront()
{
    ShopFront_pub = sf.advertise<turtlebot3_gazebo::pizzaOrder>("ShopFront", 1);
}
ShopFront::~ShopFront(){}
void ShopFront::PizzaOrder(){
    cout << "--------Pizza Shop-------------" << endl;
    cout << "Welcome to Dominos!" << endl;
    cout << "Which House? (1-8)" << endl;
    cin >> buildingId;
    cout << "How many Pizzas? (1-4)" << endl;
    cin >> pizzaNum;

    if (buildingId >= 1 && buildingId <= 8 && pizzaNum >= 1 && pizzaNum <= 4){
        // Publish Order
        turtlebot3_gazebo::pizzaOrder ShopFront;
        ShopFront.buildingId = buildingId;
        ShopFront.pizzaNum = pizzaNum;
        ShopFront_pub.publish(ShopFront);
        ROS_INFO("Building = %d", buildingId);      // Print Message in Terminal
        ROS_INFO("Pizza = %d", pizzaNum);           // Print Message in Terminal
        ros::spinOnce();                            // ROS Publish
    }
    else {
        cout << "Invalid Order" << endl;
    }
}

