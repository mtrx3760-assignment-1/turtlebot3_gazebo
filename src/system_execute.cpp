#include "turtlebot3_gazebo/robot.h"
#include "turtlebot3_gazebo/restaurant.h"
#include "ros/ros.h"

int main(int argc, char **argv){
    Restaurant Dominos;
    Robot      DeliveryBot;

    ros::init(argc,argv,"ShopFront"); 
    ros::init(argc,argv,"ShopManager");   
    ros::Rate loop_rate(10);

    while(ros::ok()){
        ros::spinOnce();    
        Dominos.ShopFront_.PizzaOrder();
        loop_rate.sleep(); 
    }
}