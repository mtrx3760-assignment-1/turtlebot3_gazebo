#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <geometry_msgs/Twist.h>


static const std::string OPENCV_WINDOW = "Image window";

class LineFollower
{
  ros::NodeHandle nh_;
  image_transport::ImageTransport it_;
  image_transport::Subscriber image_sub_;
  image_transport::Publisher image_pub_;

  ros::Publisher cmd_vel_pub_;

private:
  geometry_msgs::Twist cmd_vel;

public:
  LineFollower()
    : it_(nh_)
  {
    // Subscrive to input video feed and publish output video feed
    image_sub_ = it_.subscribe("/camera1/image_raw", 1,
      &LineFollower::imageCb, this);
    image_pub_ = it_.advertise("/image_converter/output_video", 1);
    cmd_vel_pub_   = nh_.advertise<geometry_msgs::Twist>("/cmd_vel", 10);

    cv::namedWindow(OPENCV_WINDOW);
  }

  ~LineFollower()
  {
    cv::destroyWindow(OPENCV_WINDOW);
  }

  void imageCb(const sensor_msgs::ImageConstPtr& msg)
  {
    cv_bridge::CvImagePtr cv_ptr;
    cv_bridge::CvImagePtr cv_hsv;
    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
    //converting bgr8 to hsv color space
    cv::Mat image = cv_ptr->image;
    cv::Mat hsv;
    cv::cvtColor(image,hsv,cv::COLOR_BGR2HSV);
    // //masking the yellow hue
    cv::Mat yellowMask;
    cv::inRange(hsv, cv::Scalar(10, 100, 20), cv::Scalar(40, 255, 255),yellowMask);
    // //

    // //erase all the non-yellow pixels
    int w = image.cols;
    int h = image.rows;
    int img_top = 3*(h/4);
    int img_bot = 3*(h/4)+20;

    cv::Rect crop_region(0,img_top,w,100);
    yellowMask = yellowMask(crop_region);

    cv::Moments M = cv::moments(yellowMask);
    if(M.m00>0){
        int cx = (int)(M.m10/M.m00);
        int cy = (int)(M.m01/M.m00);
        cv::circle(image,cv::Point(cx, cy), 20, (255,0,0), -1);
        int err = cx - h/2;
        cmd_vel.linear.x = 0.2;
        cmd_vel.angular.z = -((float)(err))/100;
        // cmd_vel_pub_.publish(cmd_vel);
    }
    // Update GUI Window
    cv::imshow(OPENCV_WINDOW,yellowMask);
    cv::waitKey(3);

    // Output modified video stream
    image_pub_.publish(cv_ptr->toImageMsg());
  }

};

int main(int argc, char** argv)
{
  ros::init(argc, argv, "image_converter");
  LineFollower ic;
  ros::spin();
  return 0;
}