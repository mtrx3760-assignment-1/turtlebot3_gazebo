#include "turtlebot3_gazebo/wall_follower.h"



//Constructor for Sensor System
SensorSystem::SensorSystem(){
  // initialize variables
  tb3_pose = 0.0;
  prev_tb3_pose = 0.0;
  // initialize subscribers
  laser_scan_sub_  = nh_.subscribe("scan", 10, &SensorSystem::laserScanMsgCallBack, this);
  odom_sub_ = nh_.subscribe("odom", 10, &SensorSystem::odomMsgCallBack, this);
}
SensorSystem::~SensorSystem()
{
}

//Odometer call back function
void SensorSystem::odomMsgCallBack(const nav_msgs::Odometry::ConstPtr &msg)
{
  double siny = 2.0 * (msg->pose.pose.orientation.w * msg->pose.pose.orientation.z + msg->pose.pose.orientation.x * msg->pose.pose.orientation.y);
	double cosy = 1.0 - 2.0 * (msg->pose.pose.orientation.y * msg->pose.pose.orientation.y + msg->pose.pose.orientation.z * msg->pose.pose.orientation.z);  

	tb3_pose = atan2(siny, cosy);
}

//laser scan callback function
void SensorSystem::laserScanMsgCallBack(const sensor_msgs::LaserScan::ConstPtr &msg)
{
  laser_msg = *msg;
  std::vector<float> laser_ranges;
  laser_ranges = laser_msg.ranges;
  
  //getting the laser values
  scan[FRONT] = laser_ranges[0];
  scan[RIGHT] = laser_ranges[270];
  scan[LEFT]  =  laser_ranges[90];
}
