#include "ros/ros.h"
#include "turtlebot3_gazebo/ShopManager.h"

ShopManager::ShopManager(){
    ShopManager_ShopFront_sub = sm.subscribe("ShopFront", 1000, &ShopManager::ShopManager_ShopFront_Callback, this);
    ShopManager_Robot_sub = sm.subscribe("Robot", 1000, &ShopManager::ShopManager_Robot_Callback, this);
    ShopManager_pub = sm.advertise<turtlebot3_gazebo::pizzaOrder>("ShopManager", 1);
}
ShopManager::~ShopManager(){}
void ShopManager::ShopManager_ShopFront_Callback(const turtlebot3_gazebo::pizzaOrder::ConstPtr& ShopFront)
{
    buildingId = ShopFront->buildingId;
    pizzaNum = ShopFront->pizzaNum;
    HouseQueue.push(buildingId);    // Add Elements to Queue
    PizzaQueue.push(pizzaNum);      // Add Elements to Queue
    cout << "Order Recieved for House = " << buildingId << " Pizza = " << pizzaNum << endl;
}
void ShopManager::ShopManager_Robot_Callback(const std_msgs::Int8::ConstPtr& msg)
{
    int DeliveryState = msg->data;
    if (DeliveryState == 1){
        cout << "--------Deliver to-------------" << endl;
        buildingId = HouseQueue.front();
        pizzaNum = PizzaQueue.front();
        if (pizzaNum == 0){
            cout << "No Orders to Deliver" << endl;
        }
        else {
            cout << "Deliver to House = " << buildingId << " Pizza = " << pizzaNum << endl;
            HouseQueue.pop();                   // Remove Front of Queue
            PizzaQueue.pop();                   // Remove Front of Queue
            // Publish Order
            turtlebot3_gazebo::pizzaOrder ShopManager;
            ShopManager.buildingId = buildingId;
            ShopManager.pizzaNum = pizzaNum;
            ShopManager_pub.publish(ShopManager);
            ROS_INFO("Building = %d", buildingId);      // Print Message in Terminal
            ROS_INFO("Pizza = %d", pizzaNum);           // Print Message in Terminal
            ros::spinOnce();                            // ROS Publish
        }
    }
}


