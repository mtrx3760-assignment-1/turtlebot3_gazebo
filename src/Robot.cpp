#include "ros/ros.h"
#include "turtlebot3_gazebo/turtlebot3_drive.h"
#include "turtlebot3_gazebo/robot.h"
#include <geometry_msgs/Twist.h>
#include "std_msgs/Int8.h"
#include "turtlebot3_gazebo/tb3location.h"
#include "turtlebot3_gazebo/pizzaOrder.h"
#include <iostream>
#include <queue>
using namespace std;


Robot::Robot(){
    Robot_pub = r.advertise<std_msgs::Int8>("Robot", 1000);
    Robot_sub = r.subscribe("ShopManager", 1000, &Robot::Order_Callback, this);
    Robot_sub = r.subscribe("current_location",1000, &Robot::Location_Callback, this);
    cmd_vel_topic = r.param<std::string>("cmd_vel_topic", "");
    cmd_vel_pub = r.advertise<geometry_msgs::Twist>(cmd_vel_topic, 10);
    order_callback=false;

}
Robot::~Robot(){}
void Robot::Order_Callback(const turtlebot3_gazebo::pizzaOrder::ConstPtr& ShopFront)
{
    order_callback=true;
    CurrentOrder = *ShopFront;
    
}
void Robot::Location_Callback(const turtlebot3_gazebo::tb3location::ConstPtr& current_location)
{
    loc_building_id = current_location->buildingId;
    proximity = current_location->proximity;

    if(loc_building_id==0){
        ROS_INFO("%0.2lf meters close to PizzaShop",proximity);
        
        if (proximity < 20){
            while(!order_callback){
                cout<<"Waiting for Order....."<<endl;
                updatecommandVelocity(0.0, 0.0);  
                this->ShopArrival();  
            }
            buildingId = CurrentOrder.buildingId;
            pizzaNum = CurrentOrder.pizzaNum;
            cout << "Order Recieved for House = " << buildingId << " Pizza = " << pizzaNum << endl;
            updatecommandVelocity(0.9,0);
        }
    }
    else {       
        ROS_INFO("Delivering %d Pizzas to %d House",pizzaNum,buildingId);

        if (proximity < 10){
            updatecommandVelocity(0.0, 0.0);
            cout << pizzaNum << "- Pizzas have been delivered to House " << loc_building_id << endl;
        }
    }
}
void Robot::updatecommandVelocity(double linear, double angular) {
    geometry_msgs::Twist cmd_vel;
    cmd_vel.linear.x  = linear;
    cmd_vel.angular.z = angular;
    cmd_vel_pub.publish(cmd_vel);
}

void Robot::ShopArrival(){
    std_msgs::Int8 r_msg;           // Create Int8 msg
    r_msg.data = 1;                 // Load msg
    ROS_INFO("%d", r_msg.data);     // Print Message in Terminal
    Robot_pub.publish(r_msg);       // Publish Message
    ros::spinOnce();                // ROS Publish           
    ros::spinOnce();                // Listen
}
