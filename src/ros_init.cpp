#include "turtlebot3_gazebo/wall_follower.h"

//Functions to initalize ROS Parametets

WallFollower::WallFollower()
  : nh_priv_("~")
{
  //Init gazebo ros turtlebot3 node
  ROS_INFO("TurtleBot3 Simulation Node Init");
  auto ret = init();
  ROS_ASSERT(ret);
}

WallFollower::~WallFollower()
{
  ros::shutdown();
}

/*******************************************************************************
* Init function
*******************************************************************************/
bool WallFollower::init()
{
  // initialize variables
  escape_range       = 30 * DEG2RAD;
  check_forward_dist = 0.8;
  check_side_dist    = 0.4;

  return true;
}
 