/*******************************************************************************
* Copyright 2016 ROBOTIS CO., LTD.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*******************************************************************************/

/* Authors: Taehun Lim (Darby) */

#ifndef WALL_FOLLOWER_H_
#define WALL_FOLLOWER_H_

#include <ros/ros.h>
#include "turtlebot3_gazebo/drive_system.h"
#include "turtlebot3_gazebo/sensor_system.h"


//libraries for c++ operations
#include <iostream>
#include <cmath>
#include <algorithm>
#include <stack>


#define DEG2RAD (M_PI / 180.0)
#define RAD2DEG (180.0 / M_PI)

//WallFollower class
class WallFollower
{
 public:
  WallFollower();
  ~WallFollower();
  bool init();
  bool controlLoop();

 private:
  // ROS NodeHandle
  ros::NodeHandle nh_;
  ros::NodeHandle nh_priv_;

  //Classes
  DriveSystem drive_system_tb3;
  SensorSystem sensor_system_tb3;

  // Variables
  double escape_range;
  double check_forward_dist;
  double check_side_dist;
};
#endif // wall_follower_H_
