#ifndef ROBOT_H
#define ROBOT_H

#include "ros/ros.h"
#include "turtlebot3_gazebo/turtlebot3_drive.h"
#include <geometry_msgs/Twist.h>
#include "std_msgs/Int8.h"
#include "turtlebot3_gazebo/gps.h"
#include "turtlebot3_gazebo/tb3location.h"
#include "turtlebot3_gazebo/pizzaOrder.h"
#include <iostream>
#include <queue>
using namespace std;

class Robot{
    public:
        Robot();
        ~Robot();
        void Order_Callback(const turtlebot3_gazebo::pizzaOrder::ConstPtr& ShopFront);
        void Location_Callback(const turtlebot3_gazebo::tb3location::ConstPtr& current_location);
        void updatecommandVelocity(double linear, double angular); 
        void ShopArrival();
    private:
        int buildingId;
        int pizzaNum;
        int loc_building_id;
        double proximity;
        bool order_callback;

        GpsLocation Robot_location;
        ros::NodeHandle r;
        ros::Publisher Robot_pub;
        ros::Subscriber Robot_sub;
        ros::Publisher cmd_vel_pub;
        std::string cmd_vel_topic;
        turtlebot3_gazebo::pizzaOrder CurrentOrder;
};

#endif