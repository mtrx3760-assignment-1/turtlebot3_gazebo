#ifndef SHOP_FRONT_H
#define SHOP_FRONT_H
#include "ros/ros.h"
#include "turtlebot3_gazebo/pizzaOrder.h"
#include <iostream>
#include <queue>
using namespace std;

class ShopFront{
    public:
    ShopFront();
    ~ShopFront();
    void PizzaOrder();

    private:
    int buildingId;
    int pizzaNum;
    ros::NodeHandle sf;
    ros::Publisher ShopFront_pub;

};
#endif