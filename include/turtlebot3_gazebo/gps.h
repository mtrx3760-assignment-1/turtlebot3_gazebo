/* Authors: Hrithik Selvaraj */

#ifndef GPS_H_
#define GPS_H_

#include <ros/ros.h>
#include <apriltag_ros/AprilTagDetectionArray.h>
#include <apriltag_ros/AprilTagDetection.h>
#include "turtlebot3_gazebo/tb3location.h"

class GpsLocation
{
    public:
    GpsLocation();
    ~GpsLocation();
    bool init();

    private:
    ros::NodeHandle nh_;
    ros::NodeHandle nh_priv;

    ros::Publisher location_pub;
    ros::Subscriber tag_detection_sub;

    int tag_id;
    int building_id;
    double pose_x, pose_y;
    double proximity;
    std_msgs::Header header;
    

    void TagDetectionCallBack(const apriltag_ros::AprilTagDetectionArray::ConstPtr &msg);
    void BuildingDetection();
    void UpdateLocation();

};

#endif
