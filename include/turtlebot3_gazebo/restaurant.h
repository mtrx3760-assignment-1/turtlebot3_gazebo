#ifndef RESTAURANT_H_
#define RESTAURANT_H_

#include "ros/ros.h"
#include "turtlebot3_gazebo/ShopManager.h"
#include "turtlebot3_gazebo/ShopFront.h"

class Restaurant{
 public:
  Restaurant();
  ~Restaurant();
  ShopFront ShopFront_;
  ShopManager ShopManager_;
private:
  ros::NodeHandle nh_;
  ros::NodeHandle nh_priv_;
};



#endif