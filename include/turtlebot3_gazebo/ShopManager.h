#ifndef SHOP_MANAGER_H
#define SHOP_MANAGER_H

#include "ros/ros.h"
#include "turtlebot3_gazebo/pizzaOrder.h"
#include "std_msgs/Int8.h"
#include <iostream>
#include <queue>
using namespace std;

class ShopManager{
    public:
        ShopManager();
        ~ShopManager();
        void ShopManager_ShopFront_Callback(const turtlebot3_gazebo::pizzaOrder::ConstPtr& ShopFront);
        void ShopManager_Robot_Callback(const std_msgs::Int8::ConstPtr& msg);
    private:
        int buildingId;
        int pizzaNum;
        queue<int> HouseQueue;
        queue<int> PizzaQueue;
        ros::NodeHandle sm;
        ros::Subscriber ShopManager_ShopFront_sub;
        ros::Subscriber ShopManager_Robot_sub;
        ros::Publisher ShopManager_pub;
};



#endif