
#ifndef DRIVE_SYSTEM_H_
#define DRIVE_SYSTEM_H_

#include <ros/ros.h>

#include <geometry_msgs/Twist.h>
#include <tf/transform_listener.h>

// Define the robot direction of movement
typedef enum _ROBOT_MOVEMENT {
    STOP = 0,
    FORWARD,
    BACKWARD,
    TURN_LEFT,
    TURN_RIGHT,
    MOVE_RIGHT,
    MOVE_LEFT

} ROBOT_MOVEMENT;

class DriveSystem
{
public:
    DriveSystem();
    ~DriveSystem();
    //Drive system to set velocity for the direction
    bool drive_system(const ROBOT_MOVEMENT move_type);
    //Function to publish the velocity
    void updatecommandVelocity(double linear, double angular);

private:
    ros::NodeHandle nh_;
    ros::Publisher cmd_vel_pub;
};

#endif